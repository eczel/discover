//
//  discoverCell.swift
//  DiscoverFunction
//
//  Created by Elveen on 03/04/2019.
//  Copyright © 2019 BetaMaju. All rights reserved.
//

import UIKit


protocol discoverCellDelegate
{
    //func discoverTappedClap(_sender: discoverCell)
    func discoverTappedComment()
    
}

class discoverCell: UITableViewCell
{
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var pingShape: UIImageView!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var discoverDiscription: UITextView!
    @IBOutlet weak var discoverClap: UIButton!
    @IBOutlet weak var discoverPingShare: UIButton!
    @IBOutlet weak var discoverComment: UIButton!
    @IBOutlet weak var coverView: UIView!
    @IBOutlet weak var discoverImage: UIImageView!
    @IBOutlet weak var discriptionView: UIView!
    
    var delegate: discoverCellDelegate?
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
        
        profileImage.layer.cornerRadius = 33
        profileImage.clipsToBounds = true
        
        discoverImage.layer.cornerRadius = 20
        
        discriptionView.layer.cornerRadius = 20
        discriptionView.alpha = 0.2
        
        discoverDiscription.isScrollEnabled = false
        
        //                          ------long tap------
        let hideViewGesture = UILongPressGestureRecognizer(target: self, action: #selector(longPressed(press:)))
        hideViewGesture.minimumPressDuration = 1
        coverView.addGestureRecognizer(hideViewGesture)
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
        
    }
    
    @objc func longPressed(press : UILongPressGestureRecognizer)
    {
        self.coverView.isHidden = true
        self.discriptionView.isHidden = true
     
        let elapsed: Double = 0.0
        let delay = max(0.0, 3.0 - elapsed)
        DispatchQueue.main.asyncAfter(deadline: .now() + delay)
        {
            self.coverView.isHidden = false
            self.discriptionView.isHidden = false
        }
    }
    
//    @IBAction func clapTapped(_ sender: UIButton)
//    {
//        delegate?.discoverTappedClap(_sender: self)
//    }
    
    @IBAction func commentTapped(_ sender: UIButton)
    {
        delegate?.discoverTappedComment()
    }
}

